const express = require("express");
const ZKLib = require("zklib");

const port = 3000;

const app = express();

app.get("/", (req, res) => {
  res.send("Hello world");
});

app.get("/connect", (req, res) => {
  connect();
});

app.get("/name/:user_name", (req, res) => {
  res.status(200);
  res.set("Content-type", "text/html");
  res.send(`<html><body>
  <h1>Hello: ${req.params.user_name}</h1>
  </body></html>`);
});

function connect() {
  ZK = new ZKLib({
    ip: "192.168.5.11",
    port: 4370,
    inport: 5200,
    timeout: 5000,
  });

  // connect to access control device
  ZK.connect(function (err) {
    if (err) throw err;

    // read the time info from th device
    ZK.getTime(function (err, t) {
      // disconnect from the device
      ZK.disconnect();

      if (err) throw err;

      console.log("Device clock's time is " + t.toString());
    });
  });
}

app.listen(port, () => console.log(`Application running on port: ${port}`));
